import React from 'react';
import LoginPage from '../LoginPage';
import { Provider } from 'react-redux';
import configureStore from '../../redux/configureStore';
const store = configureStore();
function App() {
  return (
    <Provider store={store}>
      <LoginPage/>
    </Provider>
  );
}

export default App;
