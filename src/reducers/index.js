import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { connectRouter } from 'connected-react-router';

const rootReducer = history =>
  combineReducers({
    // Define reducers
    form: formReducer,
    router: connectRouter(history)
  });

export default rootReducer;
